UNITTESTS = unit_test/*_test.js
TESTS = test/*_test.js
TEST = pessoasfisicas_test

test:
	node ./test/populate_db.js
	mocha --timeout 5000 --reporter nyan $(TESTS)

unittest:
	mocha --timeout 5000 --reporter nyan $(UNITTESTS)

fasttest:
	node ./test/populate_db.js
	mocha --timeout 5000 --reporter nyan ./test/$(TEST).js

populate:
	node ./test/populate_db.js

dropdb:
	dropdb node_test

createdb:
	createdb node_test -O erp

run:
	nodemon server.js

.PHONY: test