var Sequelize = require('sequelize');
var hal = require('nor-hal');
var crc32 = require('buffer-crc32');
var _ = require('underscore');
var q = require('q');
var url = require('url');

function etag(body){
  return '"' + crc32.signed(body) + '"';
}

function returnMessage(req, res, code, text, resourceName, resourceId, error, info) {
  var selfLink = '';
  if (resourceId) {
    selfLink = req.app.buildUrl(resourceName + '.itemGet', {id: resourceId});
  }
  if (!error) error = {};
  if (!info) info = {};
  var response = new hal.Resource({message: {code: code, text: text,
    error: error, info: info}}, selfLink);
  response.link('collection', req.app.buildUrl(resourceName + '.listGet', {}));
  res.json(code, response);
}

function checkEtag(req, res, result) {
  var e = etag(JSON.stringify(result.values));
  if (e !== req.headers['if-none-match']) {
    returnMessage(req, res, 400, 'Dados foram modificados desde a última consulta, ' +
      'ou ETag não está presente. Favor atualizar e tentar novamente.',
      'pais', req.params.id, {'etag': true});
    return false;
  }
  return true;
}

exports.etag = etag;
exports.returnMessage = returnMessage;
exports.checkEtag = checkEtag;

//--------------------------------------------------------

exports.listGet = function(key, resources) {
  return function(req, res){
    var resource = resources[key];
    var urlParams = url.parse(req.url, true);
    var attributes = resource.listAttributes;
    var where = {};
    try {
      if (urlParams.query.attrs)
        attributes = JSON.parse(urlParams.query.attrs);
      if (urlParams.query.where)
        where = JSON.parse(urlParams.query.where);
    } catch (err) {
      returnMessage(req, res, 400, 'Os parâmetros da requisição são inválidos.',
        resource.name, null, null);
      return;
    }
    if (attributes.indexOf('id') === -1)
      attributes.push('id');
    resource.model.findAll({attributes: attributes, where: where}).success(function(results) {
      var response = new hal.Resource({}, req.app.buildUrl(resource.name + '.listGet', {}));
      if (!resource.listParentIsList) {
        response.link('parent',
          req.app.buildUrl(resource.listParentName + '.itemGet',
            {id: instance.values[resource.listParentName + 'Id']}));
      } else {
        response.link('parent',
          req.app.buildUrl(resource.listParentName + '.listGet', {}));
      }
      response.embed(resource.namePlural, []);
      results.forEach(function(result) {
        response.embed(resource.namePlural,
          new hal.Resource(result, req.app.buildUrl(resource.name + '.itemGet', {id: result.id})));
      });
      res.json(response, 200);
    });
  };
};
  
exports.itemGet = function(key, resources) {
  return function(req, res) {
    var resource = resources[key];
    var chainer = new Sequelize.Utils.QueryChainer();
    resource.model.find(req.params.id).success(function(instance) {
      if (!instance) {
        returnMessage(req, res, 404, resource.nameVerbose + ' não encontrada.', resource.name);
        return;
      }
      var response = new hal.Resource(instance, {
        href: req.app.buildUrl(resource.name + '.itemGet', {id: instance.id}),
        title: resource.nameVerbose + ': ' + instance.asString()
      });
      if (resource.parents) {
        resource.parents.forEach(function(parent) {
          idParent = instance.values[parent + 'Id'];
          if (idParent)
            chainer.add(
              resources[parent].model.find(idParent).success(function(instanceParent) {
                response.link(parent, {
                  href: req.app.buildUrl(parent + '.itemGet', {id: idParent}),
                  title: resources[parent].nameVerbose + ': ' + instanceParent.asString()
                });
              })
            );
        });
      }
      response.link('collection', {
        href: req.app.buildUrl(resource.name + '.listGet', {}),
        title: 'Lista de ' + resource.nameVerbosePlural
      });
      var whereClause = {};
      whereClause[resource.name + 'Id'] = instance.id;
      resource.children.forEach(function(key) {
        var child = resources[key];
        var attrib = _.without(child.listAttributes, resource.name + 'Id');
        var findClause = { where: whereClause, attributes: attrib };
        if (child.listOrder)
          findClause.order = child.listOrder;
        chainer.add(
          child.model.findAll(findClause)
            .success(function(items) {
              response.embed(child.namePlural, []);
              items.forEach(function(item) {
                response.embed(child.namePlural, new hal.Resource(item,
                  req.app.buildUrl(child.name + '.itemGet', {id: item.id})));
              });
            })
        );
      });
      chainer.run().success(function() {
        res.set('ETag', etag(JSON.stringify(instance.get())));
        res.json(200, response);
      });
    });
  };
};


exports.itemInsert = function(key, resources) {
  // TODO: assert that the operation is atomic (transaction)

  var resource = resources[key];
  return function(req, res, next) {
    resource.model.create(req.body, resource.itemAttributes)
      .success(function(instance) {
        promises = updateEmbeddeds(resource, resources, req, instance);
        q.all(promises)
        .then(function(){
          returnMessage(req, res, 201, resource.nameVerbose +
            ' criado (Id: ' + instance.id + ').',
            resource.name, instance.id, null,
            {resource: resource.name, id: instance.id});
        });
      })
      .error(function(err) {
        var msg = 'Erro ao validar dados. Veja informações adicionais.';
        if (err.sql) {
          msg = 'Erro ao gravar: ' + err.detail;
          err = {sql: err};
        }
        returnMessage(req, res, 400, msg, resource.name, null, err);
      });
  };
};

exports.itemUpdate = function(key, resources) {
  return function(req, res, next) {
    var resource = resources[key];
    resource.model.find(req.params.id).success(function(instance) {
      if (!checkEtag(req, res, instance)) return;
      instance.updateAttributes(req.body, resource.itemAttributes)
        .success(function(newInstance) {
          promises = updateEmbeddeds(resource, resources, req, instance);
          q.all(promises)
          .then(function(){
            returnMessage(req, res, 200, resource.nameVerbose + ' atualizado (Id: ' + newInstance.id + ').',
              resource.name, instance.id, null, {resource: resource.name, id: instance.id});
          });
        })
        .error(function(err) {
          var msg = 'Erro ao validar dados. Veja informações adicionais.';
          if (err instanceof Array && err.length === 1) err = err[0];
          if (err.sql) {
            msg = 'Erro ao gravar: ' + err.detail;
            err = {sql: err};
          }
          returnMessage(req, res, 400, msg, resource.name, null, err);
        });
    });
  };
};


exports.itemDelete = function(key, resources) {
  var resource = resources[key];
  return function(req, res, next) {
    // todo: only deletes if etag match
    resource.model.find(req.params.id).success(function(instance) {
      if (!checkEtag(req, res, instance)) return;
      instance.destroy().success(function() {
        returnMessage(req, res, 200, resource.nameVerbose + ' removido.', resource.name);
      })
      .error(function(err) {
        if (err.length === 1) err = err[0];
        if (err.sql) {
          err = {sql: err};
        }
        returnMessage(req, res, 400,
          'Erro ao remover ' + resource.nameVerbose + '. ' + err.detail,
          resource.name, null, err);
      });
    });
  };
};

//--------------------------------------------

function updateEmbedded(resources, child, tuples, parent, parentId, del) {
  var chainer = new Sequelize.Utils.QueryChainer();
  var deferred = q.defer();
  var inserted = [], updated = [], deleted = [];
  var whereClause = {};
  whereClause[parent + 'Id'] = parentId;
  tuples.forEach(function(tuple){
    tuple[parent + 'Id'] = parentId;
    if (!tuple.id) {
      chainer.add(
        resources[child].model.create(tuple)
          .success(function(instanceIns){
            inserted.push(instanceIns.id);
          })
      );
    } else {
      chainer.add(
        resources[child].model.find(tuple.id)
        .then(function(instance){
          chainer.add(
            instance.updateAttributes(tuple)
              .success(function(instanceUpd) {
                updated.push(instanceUpd.id);
              })
          );
        })
      );
    }
  });
  chainer.run()
  .success(function(){
    if (del) {
      whereClause.id = {not: inserted.concat(updated)};
      resources[child].model.destroy(whereClause)
        .success(function(){
          deferred.resolve();
        });
    } else {
      deferred.resolve();
    }
  });
  return deferred.promise;
}

function updateEmbeddeds(resource, resources, req, instance) {
  var embeddeds = req.body._embedded;
  var promises = [];
  if (req.headers['x-insert-update-embeddeds'] && embeddeds) {
    resource.children.forEach(function(child){
      if (Object.keys(embeddeds).indexOf(resources[child].namePlural) > -1) {
        promises.push(
          updateEmbedded(resources, child,
            embeddeds[resources[child].namePlural],
            resource.name, instance.id,
            req.headers['x-delete-embeddeds'])
        );
      }
    });
  }
  return promises;
}
