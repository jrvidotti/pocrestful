var sprintf = require("sprintf-js").sprintf;

module.exports = function(sequelize, DataTypes) {

  var Bairro = sequelize.define('Bairro', {
    nome: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {notNull: true, notEmpty: true},
    },
    cidadeId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: "cidades",
      referencesKey: "id",
      validate: {notNull: true},
    }
  },{
    tableName: 'bairros',
    createdAt: 'criado',
    updatedAt: 'atualizado',
    instanceMethods: {
      asString: function() {
        return sprintf('%s (%d)', this.nome, this.id);
      }
    },
  });

  return Bairro;
};
