var sprintf = require("sprintf-js").sprintf;

module.exports = function(sequelize, DataTypes) {
  
  return sequelize.define('Cep', {
    cep: {
      type: DataTypes.STRING(9),
      allowNull: false,
      unique: true,
      validate: {notNull: true, notEmpty: true},
    },
    logradouroCompleto: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {notNull: true, notEmpty: true},
    },
    tipoLogradouro: {
      type: DataTypes.STRING(20),
      allowNull: false,
      validate: {notNull: true, notEmpty: true},
    },
    logradouro: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {notNull: true, notEmpty: true},
    },
    bairroId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: "bairros",
      referencesKey: "id",
      validate: {notNull: true},
    }
  },{
    tableName: 'ceps',
    createdAt: 'criado',
    updatedAt: 'atualizado',
    instanceMethods: {
      asString: function() {
        return sprintf('%s, %s (%d)', this.nome, this.logradouroCompleto, this.id);
      }
    },
  });
};
