var sprintf = require("sprintf-js").sprintf;

module.exports = function(sequelize, DataTypes) {

  var Cidade = sequelize.define('Cidade', {
    nome: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {notNull: true, notEmpty: true},
    },
    codigoIbge: {
      type: DataTypes.STRING(7),
      unique: true,
    },
    cep: {
      type: DataTypes.STRING(9),
    },
    estadoId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: "estados",
      referencesKey: "id",
      validate: {notNull: true},
    }
  },{
    tableName: 'cidades',
    createdAt: 'criado',
    updatedAt: 'atualizado',
    instanceMethods: {
      asString: function() {
        return sprintf('%s (%d)', this.nome, this.id);
      }
    },
  });

  return Cidade;
};
