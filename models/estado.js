var sprintf = require("sprintf-js").sprintf;

module.exports = function(sequelize, DataTypes) {

  var Estado = sequelize.define('Estado', {
    nome:     {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {notNull: true, notEmpty: true},
    },
    sigla: {
      type: DataTypes.STRING(2),
      allowNull: false,
      unique: 'pais_sigla',
      validate: {notNull: true, notEmpty: true, len: [2, 2]},
    },
    codigoIbge: {
      type: DataTypes.STRING(2),
      unique: true,
    },
    paisId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: "paises",
      referencesKey: "id",
      unique: 'pais_sigla',
      validate: {notNull: true},
    }
  },{
    tableName: 'estados',
    createdAt: 'criado',
    updatedAt: 'atualizado',
    instanceMethods: {
      asString: function() {
        return sprintf('%s, %s (%d)', this.nome, this.sigla, this.id);
      }
    },
  });

  return Estado;
};
