var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');
var lodash    = require('lodash');
var config    = require('../config.json');
var sequelize = new Sequelize(
                    config.db.name,
                    config.db.username,
                    config.db.password, {
                        host: config.db.host,
                        dialect: "postgres",
                        port: config.db.port,
                        //logging: false
                    });
var db        = {};

// dynamic import all files
fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return ((file.indexOf('.') !== 0) && (file !== 'index.js') && (file.slice(-3) == '.js'));
  })
  .forEach(function(file) {
    var model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

/*
// associate objects
Object.keys(db).forEach(function(modelName) {
  if (db[modelName].options.hasOwnProperty('associate')) {
    db[modelName].options.associate(db);
  }
});
*/

// export
module.exports = lodash.extend({
  sequelize: sequelize,
  Sequelize: Sequelize
}, db);
