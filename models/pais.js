var sprintf = require("sprintf-js").sprintf;

module.exports = function(sequelize, DataTypes) {
  
  var Pais = sequelize.define('Pais', {
    nome:     {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {notNull: true, notEmpty: true},
    },
    sigla: {
      type: DataTypes.STRING(2),
      allowNull: false,
      unique: true,
      validate: {notNull: true, notEmpty: true, len: [2, 2]},
    },
  },{
    tableName: 'paises',
    createdAt: 'criado',
    updatedAt: 'atualizado',
    instanceMethods: {
      asString: function() {
        return sprintf('%s, %s (%d)', this.nome, this.sigla, this.id);
      }
    },
  });

  return Pais;
};
