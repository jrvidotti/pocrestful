var sprintf = require('sprintf-js').sprintf;
var checkCpf = require('../utils').checkCpf;

module.exports = function(sequelize, DataTypes) {
  
  return sequelize.define('PessoaFisica', {
    nome: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {notNull: true, notEmpty: true},
    },
    cpf: {
      type: DataTypes.STRING(11),
      allowNull: false,
      unique: true,
      validate: {notNull: true, notEmpty: true, len: [11, 11],
        isValidCpf: function(value) {
          if (!checkCpf(value))
            throw new Error('CPF inválido!');
        }
      },
    },
    sexo: {
      type: DataTypes.ENUM('masculino', 'feminino'),
    },
    identidade: {
      type: DataTypes.STRING(20),
    },
    identidadeEmissor: {
      type: DataTypes.STRING(20),
    },
    identidadeData: {
      type: DataTypes.DATE
    },
    dataNascimento: {
      type: DataTypes.DATE
    },
    nomePai: {
      type: DataTypes.STRING,
    },
    nomeMae: {
      type: DataTypes.STRING,
    },
    paisId: {
      type: DataTypes.INTEGER,
      references: 'paises',
      referencesKey: 'id',
    },
    estadoId: {
      type: DataTypes.INTEGER,
      references: 'estados',
      referencesKey: 'id',
    },
    cidadeId: {
      type: DataTypes.INTEGER,
      references: 'cidades',
      referencesKey: 'id',
    },
    bairroId: {
      type: DataTypes.INTEGER,
      references: 'bairros',
      referencesKey: 'id',
    },
    cep: {
      type: DataTypes.STRING(9),
    },
    endereco: {
      type: DataTypes.STRING,
    },
    enderecoNum: {
      type: DataTypes.STRING(10),
    },
    enderecoComplemento: {
      type: DataTypes.STRING,
    },
    email: {
      type: DataTypes.STRING,
    },
  },{
    tableName: 'pessoasfisicas',
    createdAt: 'criado',
    updatedAt: 'atualizado',
    instanceMethods: {
      asString: function() {
        return sprintf('%s, %s (%d)', this.nome, this.cpf, this.id);
      }
    },
  });
};
