var sprintf = require("sprintf-js").sprintf;

module.exports = function(sequelize, DataTypes) {
  
  return sequelize.define('PessoaFisicaTelefone', {
    telefone: {
      type: DataTypes.STRING(20),
      allowNull: false,
      validate: {notNull: true, notEmpty: true},
    },
    tipo: {
      type: DataTypes.ENUM('celular', 'residencial', 'comercial', 'fax'),
    },
    pessoafisicaId: {
      type: DataTypes.INTEGER,
      references: "pessoasfisicas",
      referencesKey: "id",
      allowNull: false,
      validate: {notNull: true},
    }
  },{
    tableName: 'pessoasfisicas_telefones',
    createdAt: 'criado',
    updatedAt: 'atualizado',
    instanceMethods: {
      asString: function() {
        return sprintf('%s %s (%d)', this.nome, this.tipo, this.id);
      }
    },
  });
};
