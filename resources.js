var models = require('./models');
var apiHelper = require('./api_helper');
var hal = require('nor-hal');

var resources = {};

resources['pais'] = {
  name: 'pais',
  namePlural: 'paises',
  nameVerbose: 'País',
  nameVerbosePlural: 'Países',
  model: models.Pais,
  parents: [],
  rootName: 'pais',
  listParentName: 'root',
  listParentIsList: true,
  listAttributes: ['id','nome','sigla'],
  itemAttributes: ['nome', 'sigla'],
  children: ['estado'],
  listMethods: ['get', 'post'],
  itemMethods: ['get', 'put', 'delete'],
};

resources['estado'] = {
  name: 'estado',
  namePlural: 'estados',
  nameVerbose: 'Estado',
  nameVerbosePlural: 'Estados',
  model: models.Estado,
  parents: ['pais'],
  rootName: 'pais',
  listParentName: 'pais',
  listParentIsList: true,
  listAttributes: ['id', 'nome', 'sigla', 'paisId'],
  itemAttributes: ['nome', 'sigla', 'paisId', 'codigoIbge'],
  children: ['cidade'],
  listMethods: ['get', 'post'],
  itemMethods: ['get', 'put', 'delete'],
};

resources['cidade'] = {
  name: 'cidade',
  namePlural: 'cidades',
  nameVerbose: 'Cidade',
  nameVerbosePlural: 'Cidades',
  model: models.Cidade,
  parents: ['estado'],
  rootName: 'pais',
  listParentName: 'estado',
  listParentIsList: true,
  listAttributes: ['id', 'nome', 'estadoId'],
  itemAttributes: ['nome', 'sigla', 'estadoId', 'codigoIbge', 'cep'],
  children: ['bairro'],
  listMethods: ['get', 'post'],
  itemMethods: ['get', 'put', 'delete'],
};

resources['bairro'] = {
  name: 'bairro',
  namePlural: 'bairros',
  nameVerbose: 'Bairro',
  nameVerbosePlural: 'Bairros',
  model: models.Bairro,
  parents: ['cidade'],
  rootName: 'pais',
  listParentName: 'cidade',
  listParentIsList: true,
  listAttributes: ['id', 'nome', 'cidadeId'],
  itemAttributes: ['nome', 'cidadeId'],
  children: ['cep'],
  listMethods: ['get', 'post'],
  itemMethods: ['get', 'put', 'delete'],
};

resources['cep'] = {
  name: 'cep',
  namePlural: 'ceps',
  nameVerbose: 'CEP',
  nameVerbosePlural: 'CEPs',
  model: models.Cep,
  parents: ['bairro'],
  rootName: 'pais',
  listParentName: 'bairro',
  listParentIsList: true,
  listAttributes: ['id', 'cep', 'logradouroCompleto'],
  itemAttributes: ['cep', 'logradouroCompleto', 'tipoLogradouro', 'logradouro', 'bairroId'],
  children: [],
  listMethods: ['get', 'post'],
  itemMethods: ['get', 'put', 'delete'],
};

resources['pessoafisica'] = {
  name: 'pessoafisica',
  namePlural: 'pessoasfisicas',
  nameVerbose: 'Pessoa Física',
  nameVerbosePlural: 'Pessoas Físicas',
  model: models.PessoaFisica,
  parents: ['pais', 'estado', 'cidade', 'bairro'],
  rootName: 'root',
  listParentName: 'root',
  listParentIsList: true,
  listAttributes: ['id', 'nome', 'cpf'],
  itemAttributes: ['nome', 'cpf', 'sexo', 'identidade', 'identidadeEmissor', 'identidadeData',
        'dataNascimento', 'nomePai', 'nomeMae', 'paisId', 'estadoId', 'cidadeId', 'bairroId',
        'cep', 'endereco', 'enderecoNum', 'enderecoComplemento', 'email'],
  children: ['pessoafisica_telefone'],
  listMethods: ['get', 'post'],
  itemMethods: ['get', 'put', 'delete'],
};

resources['pessoafisica_telefone'] = {
  name: 'pessoafisica_telefone',
  namePlural: 'pessoasfisicas_telefones',
  nameVerbose: 'Pessoa Física Telefone',
  nameVerbosePlural: 'Pessoas Físicas Telefones',
  model: models.PessoaFisicaTelefone,
  parents: ['pessoafisica'],
  rootName: 'root',
  listParentName: 'root',
  listParentIsList: true,
  listAttributes: ['id', 'pessoafisicaId', 'telefone', 'tipo'],
  listOrder: ['telefone'],
  itemAttributes: ['id', 'pessoafisicaId', 'telefone', 'tipo'],
  embeddeds: [],
  children: [],
  listMethods: ['get', 'post'],
  itemMethods: ['get', 'put', 'delete'],
  hideRoot: true,
};

var methods = {};

Object.keys(resources).forEach(function(key) {
  methods[key] = {};
  methods[key].listGet    = apiHelper.listGet(key, resources);
  methods[key].listPost   = apiHelper.itemInsert(key, resources);
  methods[key].itemGet    = apiHelper.itemGet(key, resources);
  methods[key].itemPut    = apiHelper.itemUpdate(key, resources);
  methods[key].itemDelete = apiHelper.itemDelete(key, resources);
});

methods.index = {};
methods.index.list = function(req, res){
  var response = new hal.Resource({},
    {title: 'Listagem de Recursos da API',
    href: req.app.buildUrl('root.listGet', {})});
  response.embed('recursos', []);
  Object.keys(resources).forEach(function(key) {
    resource = resources[key];
    if (!resource.hideRoot)
      response.embed('recursos', new hal.Resource(
        {nome: resource.nameVerbosePlural},
        req.app.buildUrl(resource.name + '.listGet', {})));
  });
  //res.setHeader('Access-Control-Allow-Origin', '*');
  //res.setHeader('Access-Control-Allow-Methods', ['OPTIONS', 'GET', 'POST']);
  res.json(response, 200);
};

module.exports = {
  methods: methods,
  resources: resources,
};
