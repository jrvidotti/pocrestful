var config = require('./config.json');
var express = require('express');
var models = require('./models');
var resources = require('./resources').resources;
var methods = require('./resources').methods;
var http = require('http');
var path = require('path');
var Router = require('named-routes');

var app = express();

// all environments
app.set('port', process.env.PORT || config.server.port || 9000);
app.set('title', 'REST API');
app.disable('etag');
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use('/client', express.static(path.join(__dirname, 'client/app'))); //  "public" off of current is root
//app.use(express.bodyParser());

app.use(express.basicAuth(function(user, pass, callback) {
  var result = (user === 'admin' && pass === '123');
  callback(null, result);
}));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

var router = new Router();
router.extendExpress(app);
app.buildUrl = buildUrl;

app.get('/', 'root.listGet', methods.index.list);

Object.keys(resources).forEach(function(key) {
  resource = resources[key];
  resource.listMethods.forEach(function(method) {
    if (method === 'get')
      app.get('/' + resource.namePlural,
        resource.name + '.listGet',
        methods[resource.name].listGet);
    if (method === 'post')
      app.post('/' + resource.namePlural,
        resource.name + '.listPost',
        methods[resource.name].listPost);
  });
  resource.itemMethods.forEach(function(method) {
    if (method === 'get')
      app.get('/' + resource.namePlural + '/:id',
        resource.name + '.itemGet',
        methods[resource.name].itemGet);
    if (method === 'put')
      app.put('/' + resource.namePlural + '/:id',
        resource.name + '.itemPut',
        methods[resource.name].itemPut);
    if (method === 'delete')
      app.delete('/' + resource.namePlural + '/:id',
        resource.name + '.itemDelete',
        methods[resource.name].itemDelete);
  });
});

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

function buildUrl(route, params) {
  var prefix = '';
  if (app.get('env') == 'development') {
    prefix = (!app.get('trust proxy') ? 'http' : 'https') + '://localhost:' + app.get('port');
  }
  return prefix + app._router.build(route, params);
}
