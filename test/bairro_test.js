var helpers = require('./helpers');
var models = require('../models');
var etag = require('../api_helper').etag;
var q = require('q');
var config = require('../config.json');
var supertest = require('supertest');
var should = require('chai').should();
var sch = require('./schemas');

test();

function test() {
  var api = supertest(config.tests.api);
  var user = config.tests.user;
  var password = config.tests.password;

  describe('In order to test Bairro resource', function() {
    it('run all the tests', function(done) {
      console.log('starting Bairro test');

      var cidadeId;
      models.Pais.find({where: {sigla: 'BR'}})
        .then(function(pais){
          return models.Estado.find({where: {paisId: pais.id, sigla: 'MT'}});
        })
        .then(function(estado){
          return models.Cidade.find({where: {estadoId: estado.id, nome: 'Cuiabá'}});
        })
        .then(function(cidade){
          cidadeId = cidade.id;
          qsBairro = [
            models.Bairro.find({where: {cidadeId: cidadeId, nome: 'Centro'}}),
            models.Bairro.find({where: {cidadeId: cidadeId, nome: 'Goiabeiras (ALTERAR)'}}),
            models.Bairro.find({where: {cidadeId: cidadeId, nome: 'TESTE (APAGAR)'}}),
          ];
          return q.all(qsBairro);
        })
        .then(function(results){
          var cfg = {
            api: api,
            user: user,
            password: password,
            name: 'bairro',
            namePlural: 'bairros',
            path: '/bairros',
            dataPostOk: {nome: 'CPA 4', cidadeId: cidadeId},
            dataNonUnique: null, // null value --> skip the test
            dataPutOk: {nome: 'Goiabeiras', cidadeId: cidadeId},
            dataPutEmpty: {nome: '', codigoIbge: '', cidadeId: null},
            getId: results[0].id,
            getEtag: etag(JSON.stringify(results[0])),
            putId: results[1].id,
            putEtag: etag(JSON.stringify(results[1])),
            deleteId: results[2].id,
            deleteEtag: etag(JSON.stringify(results[2])),
          };
          helpers.testGetList(cfg);
          helpers.testGetItem(cfg);
          helpers.testGetItemNotFound(cfg);
          helpers.testPost(cfg);
          helpers.testPut(cfg);
          helpers.testDelete(cfg);
          console.log('finishing Bairro test');
          done();
        });
    });
  });
}
