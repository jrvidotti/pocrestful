var helpers = require('./helpers');
var models = require('../models');
var etag = require('../api_helper').etag;
var q = require('q');
var config = require('../config.json');
var supertest = require('supertest');
var should = require('chai').should();
var sch = require('./schemas');

test();

function test() {
  var api = supertest(config.tests.api);
  var user = config.tests.user;
  var password = config.tests.password;

  describe('In order to test Cep resource', function() {
    it('run all the tests', function(done) {
      console.log('starting Cep test');

      var bairroId;
      models.Pais.find({where: {sigla: 'BR'}})
        .then(function(pais){
          return models.Estado.find({where: {paisId: pais.id, sigla: 'MT'}});
        })
        .then(function(estado){
          return models.Cidade.find({where: {estadoId: estado.id, nome: 'Cuiabá'}});
        })
        .then(function(cidade){
          return models.Bairro.find({where: {cidadeId: cidade.id, nome: 'Centro'}});
        })
        .then(function(bairro){
          bairroId = bairro.id;
          qsCep = [
            models.Cep.find({where: {bairroId: bairroId, cep: '78005-290'}}),
            models.Cep.find({where: {bairroId: bairroId, cep: '78005-150'}}),
            models.Cep.find({where: {bairroId: bairroId, cep: '99999-999'}}),
          ];
          return q.all(qsCep);
        })
        .then(function(results){
          var cfg = {
            api: api,
            user: user,
            password: password,
            name: 'cep',
            namePlural: 'ceps',
            path: '/ceps',
            dataPostOk: {cep: '78005-450', logradouroCompleto: 'Avenida Getúlio Vargas',
              tipoLogradouro: 'Avenida', logradouro: 'Getúlio Vargas',
              bairroId: bairroId},
            dataNonUnique: {cep: '78005-290', logradouroCompleto: 'Rua Joaquim Murtinho',
              tipoLogradouro: 'Rua', logradouro: 'Joaquim Murtinho',
              bairroId: bairroId},
            dataPutOk: {cep: '78005-150', logradouroCompleto: 'Rua 13 de Junho',
              tipoLogradouro: 'Rua', logradouro: '13 de Junho',
              bairroId: bairroId},
            dataPutEmpty: {cep: '', logradouroCompleto: '', tipoLogradouro: '',
              logradouro: '', bairroId: null},
            getId: results[0].id,
            getEtag: etag(JSON.stringify(results[0])),
            putId: results[1].id,
            putEtag: etag(JSON.stringify(results[1])),
            deleteId: results[2].id,
            deleteEtag: etag(JSON.stringify(results[2])),
              skipDeleteConstraint: true
          };
          helpers.testGetList(cfg);
          helpers.testGetItem(cfg);
          helpers.testGetItemNotFound(cfg);
          helpers.testPost(cfg);
          helpers.testPut(cfg);
          helpers.testDelete(cfg);
          console.log('finishing Cep test');
          done();
        });
    });
  });
}
