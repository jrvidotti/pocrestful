var helpers = require('./helpers');
var models = require('../models');
var etag = require('../api_helper').etag;
var q = require('q');
var config = require('../config.json');
var supertest = require('supertest');
var should = require('chai').should();
var sch = require('./schemas');

test();

function test() {
  var api = supertest(config.tests.api);
  var user = config.tests.user;
  var password = config.tests.password;

  describe('In order to test Cidade resource', function() {
    it('run all the tests', function(done) {
      console.log('starting Cidade test');

      var estadoId;
      models.Pais.find({where: {sigla: 'BR'}})
        .then(function(pais){
          return models.Estado.find({where: {paisId: pais.id, sigla: 'MT'}});
        })
        .then(function(estado){
          estadoId = estado.id;
          qsCidade = [
            models.Cidade.find({where: {estadoId: estadoId, nome: 'Cuiabá'}}),
            models.Cidade.find({where: {estadoId: estadoId, nome: 'Chapada dos Guimarães (ALTERAR)'}}),
            models.Cidade.find({where: {estadoId: estadoId, nome: 'TESTE (APAGAR)'}}),
          ];
          return q.all(qsCidade);
        })
        .then(function(results){
          var cfg = {
            api: api,
            user: user,
            password: password,
            name: 'cidade',
            namePlural: 'cidades',
            path: '/cidades', // the base path to resource
            dataPostOk: {nome: 'Sinop', codigoIbge: '1234567', estadoId: estadoId},
            dataNonUnique: {nome: 'Cuiabá (repet.)', codigoIbge: '5103403', estadoId: estadoId},
            dataPutOk: {nome: 'Chapada dos Guimarães', estadoId: estadoId},
            dataPutEmpty: {nome: '', codigoIbge: '', estadoId: null},
            getId: results[0].id,
            getEtag: etag(JSON.stringify(results[0])),
            putId: results[1].id,
            putEtag: etag(JSON.stringify(results[1])),
            deleteId: results[2].id,
            deleteEtag: etag(JSON.stringify(results[2])),
          };
          helpers.testGetList(cfg);
          helpers.testGetItem(cfg);
          helpers.testGetItemNotFound(cfg);
          helpers.testPost(cfg);
          helpers.testPut(cfg);
          helpers.testDelete(cfg);
          console.log('finishing Cidade test');
          done();
        });
    });
  });
}
