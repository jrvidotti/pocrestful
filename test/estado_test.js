var helpers = require('./helpers');
var models = require('../models');
var etag = require('../api_helper').etag;
var q = require('q');
var config = require('../config.json');
var supertest = require('supertest');
var should = require('chai').should();
var sch = require('./schemas');

test();

function test() {
  var api = supertest(config.tests.api);
  var user = config.tests.user;
  var password = config.tests.password;

  describe('In order to test Estado resource', function() {
    it('run all the tests', function(done) {

      console.log('starting Estado test');
      var paisId;
      models.Pais.find({where: {sigla: "BR"}})
        .then(function(pais){
          paisId = pais.id;
          qsEstado = [
            models.Estado.find({where: {paisId: paisId, sigla: "MT"}}),
            models.Estado.find({where: {paisId: paisId, sigla: "AP"}}),
            models.Estado.find({where: {paisId: paisId, sigla: "AL"}}),
          ];
          return q.all(qsEstado);
        })
        .then(function(results){
          var cfg = {
            api: api,
            user: user,
            password: password,
            name: 'estado',
            namePlural: 'estados',
            path: '/estados', // the base path to resource
            dataPostOk: {nome: 'Roraima', sigla: 'RO', paisId: paisId},
            dataNonUnique: {nome: 'Mato Grosso', sigla: 'MT', paisId: paisId},
            dataPutOk: {nome: 'Ceará', sigla: 'CE', paisId: paisId},
            dataPutEmpty: {nome: '', sigla: '', paisId: null},
            getId: results[0].id,
            getEtag: etag(JSON.stringify(results[0])),
            putId: results[1].id,
            putEtag: etag(JSON.stringify(results[1])),
            deleteId: results[2].id,
            deleteEtag: etag(JSON.stringify(results[2])),
          };
          helpers.testGetList(cfg);
          helpers.testGetItem(cfg);
          helpers.testGetItemNotFound(cfg);
          helpers.testPost(cfg);
          helpers.testPut(cfg);
          helpers.testDelete(cfg);
          console.log('finishing Estado test');
          done();
        });
    });
  });
}
