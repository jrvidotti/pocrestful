var Sequelize = require('sequelize');
var sch = require('./schemas');
var _ = require('underscore');
var q = require('q');
var models = require('../models');

function findPlace(siglaPais, siglaEstado, nomeCidade, nomeBairro) {
  var deferred = q.defer();
  var tmp = {};
  models.Pais.find({where: {sigla: siglaPais}})
    .then(function(pais){
      if (!pais) deferred.resolve(tmp);
      tmp.paisId = pais.id;
      return models.Estado.find({where: {paisId: pais.id, sigla: siglaEstado}});
    })
    .then(function(estado){
      if (!estado) deferred.resolve(tmp);
      tmp.estadoId = estado.id;
      return models.Cidade.find({where: {estadoId: estado.id, nome: nomeCidade}});
    })
    .then(function(cidade){
      if (!cidade) deferred.resolve(tmp);
      tmp.cidadeId = cidade.id;
      return models.Bairro.find({where: {cidadeId: cidade.id, nome: nomeBairro}});
    })
    .then(function(bairro){
      if (!bairro) deferred.resolve(tmp);
      tmp.bairroId = bairro.id;
      deferred.resolve(tmp);
    });
  return deferred.promise;
}

function getData(api, path, user, password, callback) {
  api.get(path)
    .set('Accept', 'application/json')
    .auth(user, password)
    .end(callback);
}

function testCredentials(api, path, user, password) {
  it('fails if wrong credentials are provided', function(done) {
    api.get(path)
    .set('Accept', 'application/json')
    .auth('incorrect', 'credentials')
    .expect(401, done);
  });
  it('fails if no credentials are provided', function(done) {
    api.get(path)
    .set('Accept', 'application/json')
    .expect(401, done);
  });
}

function requiredAttributes(schemaList) {
  var required = [];
  schemaList.forEach(function(schema) {
    var reqs = _.without(sch[schema].required, '_embedded');
    reqs = _.without(reqs, '_links');
    reqs.forEach(function(req) {
      if (_.contains(sch[schema].properties[req].type, 'null')) {
        reqs = _.without(reqs, req);
      }
    });
    required = required.concat(reqs);
  });
  return required;
}

//-------------------------------------------

function testGetList(cfg) {
  describe('GET ' + cfg.path, function() {
    testCredentials(cfg.api, cfg.path, cfg.user, cfg.password);
    testResponseListOk(cfg);
    if (cfg.attrSelect)
      testResponseListOk(cfg, true);
  });
}

function testResponseListOk(cfg, doAttrSelect) {
  var path = cfg.path;
  var text = 'should respond with a ok list resource';
  var jsonSchemaName = cfg.name + '_list';
  if (doAttrSelect) {
    path = path + cfg.attrSelect;
    text = text + ' with column selection';
    jsonSchemaName = jsonSchemaName + '_custom';
  }
  it(text, function(done) {
    cfg.api.get(path)
      .set('Accept', 'application/json')
      .auth(cfg.user, cfg.password)
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.should.be.jsonSchema(sch.hal);
        if (doAttrSelect) {
          sch.should.have.property(jsonSchemaName);
        }
        if (sch[jsonSchemaName])
          res.body.should.be.jsonSchema(sch[jsonSchemaName]);
        res.body.should.have.property('_embedded');
        res.body._embedded.should.have.property(cfg.namePlural);
        res.body._embedded[cfg.namePlural].should.be.an.Array;
        done();
      });
  });
}

//----------------------------

function testGetItem(cfg) {
  describe('GET ' + cfg.path + '/' + cfg.getId, function() {
    testCredentials(cfg.api, cfg.path + '/' + cfg.getId, cfg.user, cfg.password);
    testGetItemOk(cfg);
  });
}

function testGetItemOk(cfg) {
  it('should respond with a item resource', function(done) {
    cfg.api.get(cfg.path + '/' + cfg.getId)
      .set('Accept', 'application/json')
      .auth(cfg.user, cfg.password)
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.should.be.jsonSchema(sch.hal)
          .jsonSchema(sch.default)
          .jsonSchema(sch[cfg.name + '_basic'])
          .jsonSchema(sch[cfg.name + '_full']);
        res.body.id.should.be.equal(cfg.getId);
        done();
      });
  });
}

//----------------------------

function testGetItemNotFound(cfg) {
  describe('GET ' + cfg.path + '/999', function() {
    testGetItemNotFoundResponse(cfg);
  });
}

function testGetItemNotFoundResponse(cfg) {
  it('should have a 404 response', function(done) {
    cfg.api.get(cfg.path + '/999')
      .set('Accept', 'application/json')
      .auth(cfg.user, cfg.password)
      .expect('Content-Type', /json/)
      .expect(404)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.should.be.jsonSchema(sch.hal)
          .jsonSchema(sch.message);
        done();
      });
  });
}

//----------------------------

function testPost(cfg) {
  describe('POST ' + cfg.path, function() {
    // cfg.dataNonUnique is deprecated. Use cfg.postValidationError instead.
    if (cfg.dataNonUnique)
      testPostValidationError(cfg, cfg.dataNonUnique, ['sql']);

    // testPostEmptyForm is deprecated. Use cfg.postValidationError instead.
    if (!cfg.validationError) {
      var requiredAttr = requiredAttributes(
        [cfg.name + '_basic', cfg.name + '_full']);
      testPostValidationError(cfg, null, requiredAttr);
    }

    if (cfg.validationError) {
      cfg.validationError.forEach(function(item){
        if (item.post)
          testPostValidationError(cfg, item.data, item.errors);
      });
    }

    // TODO: Test POST on item should return 405 METHOD NOT ALLOWED
    testPostOk(cfg);
  });
}

function testPostValidationError(cfg, data, errors) {
  it('validation error', function(done) {
    cfg.api.post(cfg.path)
    .set('Accept', 'application/json')
    .auth(cfg.user, cfg.password)
    .send(data)
    .expect(400)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      res.body.should.be.a.jsonSchema(sch.hal);
      res.body.should.be.a.jsonSchema(sch.message);
      var errorAttributes = Object.keys(res.body.message.error);
      errorAttributes.should.eql(errors);
      done();
    });
  });
}

function testPostEmptyForm(cfg) {
  it('error with empty form', function(done) {
    cfg.api.post(cfg.path)
    .set('Accept', 'application/json')
    .auth(cfg.user, cfg.password)
    .send(null)
    .expect(400)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      res.body.should.be.a.jsonSchema(sch.hal);
      res.body.should.be.a.jsonSchema(sch.message);
      var errorAttributes = Object.keys(res.body.message.error);
      var requiredAttr = requiredAttributes(
        [cfg.name + '_basic', cfg.name + '_full']);
      errorAttributes.should.eql(requiredAttr);
      done();
    });
  });
}

function testPostOk(cfg) {
  it('inserts a new item', function(done) {
    cfg.api.post(cfg.path)
    .set('Accept', 'application/json')
    .set('x-insert-update-embeddeds', true)
    .set('x-delete-embeddeds', true)
    .auth(cfg.user, cfg.password)
    .send(cfg.dataPostOk)
    .expect(201)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      res.body.should.be.a.jsonSchema(sch.hal);
      res.body.should.be.a.jsonSchema(sch.message);
      newId = res.body.message.info.id;
      newId.should.be.not.empty;
      getData(cfg.api, cfg.path + '/' + newId, cfg.user, cfg.password, function(err, res2) {
        expected = JSON.parse(JSON.stringify(cfg.dataPostOk));
        if (cfg.embeddeds) {
          var _embedded = expected._embedded;
          delete expected._embedded;
        }
        res2.body.should.contain(expected);
        if (cfg.embeddeds) {
          cfg.embeddeds.forEach(function(embedded){
            for (var i = 0; i < res2.body._embedded[embedded].length; i++) {
              res2.body._embedded[embedded][i].should.contain(_embedded[embedded][i]);
            }
          });
        }
        done();
      });
    });
  });
}

//-----------------------------------------

function testPut(cfg) {
  describe('PUT ' + cfg.path + '/' + cfg.putId, function() {
    // cfg.dataPutEmpty is deprecated. Use cfg.postValidationError instead.
    if (cfg.dataPutEmpty) {
      var requiredAttr = requiredAttributes(
        [cfg.name + '_basic', cfg.name + '_full']);
      testPutValidationError(cfg, cfg.dataPutEmpty, requiredAttr);
    }

    // cfg.dataNonUnique is deprecated. Use cfg.postValidationError instead.
    if (cfg.dataNonUnique)
      testPutValidationError(cfg, cfg.dataNonUnique, ['sql']);

    if (cfg.validationError) {
      cfg.validationError.forEach(function(item){
        if (item.put)
          testPutValidationError(cfg, item.data, item.errors);
      });
    }
    testPutWrongEtag(cfg);
    // TODO: Test PUT on list should return 405 METHOD NOT ALLOWED
    // TODO: Test PUT on item not found should return 404 NOT FOUND
    testPutOk(cfg);
  });
}

function testPutWrongEtag(cfg) {
  it('fails trying to update with wrong etag', function(done) {
    cfg.api.put(cfg.path + '/' + cfg.putId)
    .set('Accept', 'application/json')
    .set('If-None-Match', '"wrong etag"')
    .auth(cfg.user, cfg.password)
    .send(cfg.dataPutOk)
    .expect(400)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      res.body.should.be.a.jsonSchema(sch.hal);
      res.body.should.be.a.jsonSchema(sch.message);
      errorAttributes = Object.keys(res.body.message.error);
      errorAttributes.should.eql(['etag']);
      done();
    });
  });
}

function testPutValidationError(cfg, data, error) {
  it('fails trying to update with empty form', function(done) {
    cfg.api.put(cfg.path + '/' + cfg.putId)
    .set('Accept', 'application/json')
    .set('If-None-Match', cfg.putEtag)
    .auth(cfg.user, cfg.password)
    .send(data)
    .expect(400)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      res.body.should.be.a.jsonSchema(sch.hal);
      res.body.should.be.a.jsonSchema(sch.message);
      var errorAttributes = Object.keys(res.body.message.error);
      errorAttributes.should.eql(error);
      done();
    });
  });
}

function testPutOk(cfg) {
  it('updates an existing item', function(done) {
    cfg.api.put(cfg.path + '/' + cfg.putId)
    .set('Accept', 'application/json')
    .set('If-None-Match', cfg.putEtag)
    .set('x-insert-update-embeddeds', true)
    .set('x-delete-embeddeds', true)
    .auth(cfg.user, cfg.password)
    .send(cfg.dataPutOk)
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      res.body.should.be.a.jsonSchema(sch.hal);
      res.body.should.be.a.jsonSchema(sch.message);
      res.body.message.info.id.should.be.equal(cfg.putId);
      getData(cfg.api, cfg.path + '/' + cfg.putId, cfg.user, cfg.password, function(err, res2) {
        expected = JSON.parse(JSON.stringify(cfg.dataPutOk));
        if (cfg.embeddeds) {
          var _embedded = expected._embedded;
          delete expected._embedded;
        }
        res2.body.should.contain(expected);
        if (cfg.embeddeds) {
          cfg.embeddeds.forEach(function(embedded){
            for (var i = 0; i < res2.body._embedded[embedded].length; i++) {
              res2.body._embedded[embedded][i].should.contain(_embedded[embedded][i]);
            }
          });
        }
        done();
      });
    });
  });
}

//---------------------------------

function testDelete(cfg) {
  describe('DELETE ' + cfg.path + '/' + cfg.deleteId, function() {
    testDeleteWrongEtag(cfg);
    testDeleteOk(cfg);
    testDeleteConstraintError(cfg);
  });
}

function testDeleteWrongEtag(cfg) {
  it('fails trying to delete with wrong etag', function(done) {
    cfg.api.del(cfg.path + '/' + cfg.deleteId)
    .set('Accept', 'application/json')
    .set('If-None-Match', '"wrong etag"')
    .auth(cfg.user, cfg.password)
    .expect(400)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      res.body.should.be.a.jsonSchema(sch.hal);
      res.body.should.be.a.jsonSchema(sch.message);
      errorAttributes = Object.keys(res.body.message.error);
      errorAttributes.should.eql(['etag']);
      done();
    });
  });
}

function testDeleteOk(cfg) {
  it('deletes an existing item', function(done) {
    cfg.api.del(cfg.path + '/' + cfg.deleteId)
    .set('Accept', 'application/json')
    .set('If-None-Match', cfg.deleteEtag)
    .auth(cfg.user, cfg.password)
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      res.body.should.be.a.jsonSchema(sch.hal);
      res.body.should.be.a.jsonSchema(sch.message);
      getData(cfg.api, cfg.path + '/' + cfg.deleteId, cfg.user, cfg.password, function(err, res2) {
        res2.statusCode.should.be.equal(404);
        done();
      });
    });
  });
}

function testDeleteConstraintError(cfg) {
  if (!cfg.skipDeleteConstraint)
    it('trying to delete a item with dependencies', function(done) {
      cfg.api.del(cfg.path + '/' + cfg.getId)
      .set('Accept', 'application/json')
      .set('If-None-Match', cfg.getEtag)
      .auth(cfg.user, cfg.password)
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.should.be.a.jsonSchema(sch.hal);
        res.body.should.be.a.jsonSchema(sch.message);
        errorAttributes = Object.keys(res.body.message.error);
        errorAttributes.should.eql(['sql']);
        done();
      });
    });
}

module.exports = {
  findPlace: findPlace,
  testGetList: testGetList,
  testGetItem: testGetItem,
  testGetItemNotFound: testGetItemNotFound,
  testPost: testPost,
  testPut: testPut,
  testDelete: testDelete,
};