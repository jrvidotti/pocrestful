var helpers = require('./helpers');
var models = require('../models');
var etag = require('../api_helper').etag;
var q = require('q');
var config = require('../config.json');
var supertest = require('supertest');
var should = require('chai').should();
var sch = require('./schemas');

test();

function test() {
  var api = supertest(config.tests.api);
  var user = config.tests.user;
  var password = config.tests.password;

  describe('In order to test Pais resource', function() {
    it('run all the tests', function(done) {

      console.log('starting Pais test');
      var queries = [
        models.Pais.find({where: {sigla: "BR"}}),
        models.Pais.find({where: {sigla: "UY"}}),
        models.Pais.find({where: {sigla: "XX"}}),
      ];
      q.all(queries).then(function(results) {
        var cfg = {
          api: api,
          user: user,
          password: password,
          name: 'pais',
          namePlural: 'paises',
          path: '/paises', // the base path to resource
          dataPostOk: {nome: 'Alemanha', sigla: 'DE'},
          dataNonUnique: {nome: 'Brasil', sigla: 'BR'},
          dataPutOk: {nome: 'Uruguai', sigla: 'UY'},
          dataPutEmpty: {nome: '', sigla: ''},
          getId: results[0].id,
          getEtag: etag(JSON.stringify(results[0])),
          putId: results[1].id,
          putEtag: etag(JSON.stringify(results[1])),
          deleteId: results[2].id,
          deleteEtag: etag(JSON.stringify(results[2])),
        };
        helpers.testGetList(cfg);
        helpers.testGetItem(cfg);
        helpers.testGetItemNotFound(cfg);
        helpers.testPost(cfg);
        helpers.testPut(cfg);
        helpers.testDelete(cfg);
        console.log('finishing Pais test');
        done();
      });
    });
  });
}
