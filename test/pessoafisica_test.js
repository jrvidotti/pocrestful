var helpers = require('./helpers');
var models = require('../models');
var etag = require('../api_helper').etag;
var q = require('q');
var config = require('../config.json');
var supertest = require('supertest');
var should = require('chai').should();
var sch = require('./schemas');

test();

function test() {
  var api = supertest(config.tests.api);
  var user = config.tests.user;
  var password = config.tests.password;

  describe('In order to test Pessoa Fisica resource', function() {
    it('run all the tests', function(done) {
      console.log('starting Pessoa Fisica test');

      helpers.findPlace('BR', 'MT', 'Cuiabá', 'Centro').then(function(local) {
        qsPF = [
          models.PessoaFisica.find({where: {cpf: '11111111111'}}),
          models.PessoaFisica.find({where: {cpf: '22222222222'}}),
          models.PessoaFisica.find({where: {cpf: '99999999999'}}),
        ];
        q.all(qsPF)
          .then(function(results){
            var cfg = {
              api: api,
              user: user,
              password: password,
              name: 'pessoafisica',
              namePlural: 'pessoasfisicas',
              path: '/pessoasfisicas',
              getId: results[0].id,
              getEtag: etag(JSON.stringify(results[0])),
              putId: results[1].id,
              putEtag: etag(JSON.stringify(results[1])),
              deleteId: results[2].id,
              deleteEtag: etag(JSON.stringify(results[2])),
              skipDeleteConstraint: true,
              embeddeds: ['pessoasfisicas_telefones'],
              attrSelect: '?attrs=["id","nome","cpf","dataNascimento","nomePai","nomeMae"]',
              where: '?query={"cpf":"33333333333"}',
            };
            cfg.dataPostOk = {nome: 'Beltrano da Silva', cpf: '33333333333', sexo: 'masculino',
              identidade: "9234567-8", identidadeEmissor: "SSP/MT",
              identidadeData: new Date('1998-03-05'), dataNascimento: new Date('1969-05-11'),
              nomePai: "João da Silva", nomeMae: "Maria da Silva", email: "beltrano@gmail.com",
              cep: '78005-290', endereco: 'Rua Joaquim Murtinho', enderecoNum: '674',
              enderecoComplemento: 'Apartamento 304', bairroId: local.bairroId,
              cidadeId: local.cidadeId, estadoId: local.estadoId, paisId: local.paisId,
              _embedded: {
                pessoasfisicas_telefones: [
                  {telefone: "(65)2222-7156", tipo: "comercial"},
                  {telefone: "(65)3333-4871", tipo: "residencial"},
                  {telefone: "(65)9999-4852", tipo: "celular"},
                ]}
              };
            cfg.dataPutOk = {nome: 'Fulano de Tal', cpf: '22222222222', sexo: 'masculino',
              identidade: "555555-8", identidadeEmissor: "SSP/MT", identidadeData: new Date('1993-03-05'),
              dataNascimento: new Date('1975-03-23'), nomePai: "José de Tal",
              nomeMae: "Creuza de Tal", email: "fulano@gmail.com",
              _embedded: {
                pessoasfisicas_telefones: [
                  {telefone: "(65)3333-3333", tipo: "residencial"},
                  {telefone: "(65)8888-8888", tipo: "celular"},
                ]}
              };
            cfg.validationError = [
              {post: true, put: true, data: {nome: 'TESTE CPF INVALIDO', cpf: '11111111112'}, errors: ['cpf']},
              {post: true, put: true, data: {nome: '', cpf: ''}, errors: ['nome', 'cpf']},
              {post: true, put: false, data: null, errors: ['nome', 'cpf']},
              {post: true, put: true, data: {nome: 'TESTE NON UNIQUE (CPF)', cpf: '11111111111'}, errors: ['sql']},
              ];

            helpers.testGetList(cfg);
            helpers.testGetItem(cfg);
            helpers.testGetItemNotFound(cfg);
            helpers.testPost(cfg);
            helpers.testPut(cfg);
            helpers.testDelete(cfg);

            console.log('finishing Pessoa Fisica test');
            done();
          });
      });
    });
  });
}
