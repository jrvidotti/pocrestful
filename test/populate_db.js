var models = require('../models');

var paises = [
    {nome: 'Brasil', sigla: 'BR', estados: [
      {nome: 'Mato Grosso', sigla: 'MT', codigoIbge: 51, cidades: [
        {nome: 'Cuiabá', codigoIbge: '5103403', bairros: [
          {nome: 'Centro', ceps: [
            {cep: '78005-290', logradouroCompleto: 'Rua Joaquim Murtinho', tipoLogradouro: 'Rua', logradouro: 'Joaquim Murtinho'},
            {cep: '78005-150', logradouroCompleto: 'ATUALIZAR', tipoLogradouro: 'ATUALIZAR', logradouro: 'ATUALIZAR'},
            {cep: '99999-999', logradouroCompleto: 'APAGAR', tipoLogradouro: 'APAGAR', logradouro: 'APAGAR'},
          ]},
          {nome: 'CPA 1', ceps: [
            {cep: '78055-105', logradouroCompleto: 'Rua Sorocaba', tipoLogradouro: 'Rua', logradouro: 'Sorocaba'},
          ]},
          {nome: 'Goiabeiras (ALTERAR)', ceps: [
            {cep: '78032-160', logradouroCompleto: 'Av. São Sebastião (número 1200-3600)', tipoLogradouro: 'Av.', logradouro: 'São Sebastião'},
          ]},
          {nome: 'TESTE (APAGAR)'}
        ]},
        {nome: 'Chapada dos Guimarães (ALTERAR)'},
        {nome: 'TESTE (APAGAR)'},
        {nome: 'Várzea Grande', bairros: [
          {nome: 'Centro'},
          {nome: 'Cristo Rei'},
          {nome: 'Parque do Lago'},
          {nome: 'Nova Várzea Grande'},
        ]},
      ]},
      {nome: 'São Paulo', sigla: 'SP', codigoIbge: 35, cidades: [
        {nome: 'São Paulo'},
        {nome: 'Santos'},
      ]},
      {nome: 'Amapá', sigla: 'AP', cidades: []},
      {nome: 'Alagoas', sigla: 'AL', cidades: []},
      {nome: 'Paraná', sigla: 'PR', codigoIbge: 41, cidades: [
        {nome: 'Curitiba'},
        {nome: 'Maringá'},
        {nome: 'Paranavaí'},
      ]},
    ]},
    {nome: 'Estados Unidos', sigla: 'US'},
    {nome: 'Argentina', sigla: 'AR'},
    {nome: 'Uruguay (alterar para Uruguai)', sigla: 'UY'},
    {nome: 'APAGAR', sigla: 'XX'},
];

var pessoasfisicas = [
  {nome: 'Junior Vidotti', cpf: '11111111111', sexo: 'masculino',
  identidade: "1234567-8", identidadeEmissor: "SSP/MT", identidadeData: new Date('1995-03-05'),
  dataNascimento: new Date('1979-01-16'), nomePai: "Antonio Ademar Vidotti",
  nomeMae: "Marileide Gobato Vidotti", email: "jrvidotti@gmail.com",
  pessoasfisicas_telefones: [
    {telefone: "(65)9205-4852", tipo: "celular"},
    {telefone: "(65)3052-4871", tipo: "residencial"},
    {telefone: "(65)3051-7156", tipo: "comercial"},
  ]},
  {nome: 'ALTERAR', cpf: '22222222222', sexo: 'masculino',
  identidade: "9876123-8", identidadeEmissor: "SSP/MT", identidadeData: new Date('1995-03-05'),
  dataNascimento: new Date('1979-11-16'), nomePai: "ALTERAR",
  nomeMae: "ALTERAR", email: "jrvidotti@gmail.com",
  pessoasfisicas_telefones: [
    {telefone: "(65)8888-8888", tipo: "celular"},
    {telefone: "(65)4444-4444", tipo: "residencial"},
  ]},
  {nome: 'APAGAR', cpf: '99999999999'},
];

models.sequelize
  .sync({force: true})
  .complete(function(err) {
    if (!!err) {
      console.log('Error while creating tables:', err);
    } else {
      //console.log('Tables created successfully.');
      populatePaises(paises);
      if (pessoasfisicas) {
        populatePessoasFisicas(pessoasfisicas);
      }
    }
  });

function populatePaises(lista) {
  lista.forEach(function(item){
    var obj = models.Pais.build(item);
    obj.save().complete(function(err) {
      if (!!err) {
        console.log('Error while saving:', err);
      } else {
        //console.log('Saved:', obj.asString());
        if (item.estados) {
          populateEstados(item.estados, obj);
        }
      }
    });
  });
}

function populateEstados(lista, pais) {
  lista.forEach(function(item){
    item.paisId = pais.id;
    var obj = models.Estado.build(item);
    obj.save().complete(function(err) {
      if (!!err) {
        console.log('Error while saving:', err);
      } else {
        //console.log('Saved:', obj.asString());
        if (item.cidades) {
          populateCidades(item.cidades, obj);
        }
      }
    });
  });
}

function populateCidades(lista, estado) {
  lista.forEach(function(item){
    item.estadoId = estado.id;
    var obj = models.Cidade.build(item);
    obj.save().complete(function(err) {
      if (!!err) {
        console.log('Error while saving:', err);
      } else {
        //console.log('Saved:', obj.asString());
        if (item.bairros) {
          populateBairros(item.bairros, obj);
        }
      }
    });
  });
}

function populateBairros(lista, cidade) {
  lista.forEach(function(item){
    item.cidadeId = cidade.id;
    var obj = models.Bairro.build(item);
    obj.save().complete(function(err) {
      if (!!err) {
        console.log('Error while saving:', err);
      } else {
        if (item.ceps) {
          populateCeps(item.ceps, obj);
        }
      }
    });
  });
}

function populateCeps(lista, bairro) {
  lista.forEach(function(item){
    item.bairroId = bairro.id;
    var obj = models.Cep.build(item);
    obj.save().complete(function(err) {
      if (!!err) {
        console.log('Error while saving:', err);
      } else {
      }
    });
  });
}

function populatePessoasFisicas(lista) {
  lista.forEach(function(item){
    var obj = models.PessoaFisica.build(item);
    obj.save().complete(function(err) {
      if (!!err) {
        console.log('Error while saving:', err);
      } else {
        if (item.pessoasfisicas_telefones) {
          populatePessoasFisicasTelefones(item.pessoasfisicas_telefones, obj);
        }
      }
    });
  });
}

function populatePessoasFisicasTelefones(lista, pf) {
  lista.forEach(function(item){
    item.pessoafisicaId = pf.id;
    var obj = models.PessoaFisicaTelefone.build(item);
    obj.save().complete(function(err) {
      if (!!err) {
        console.log('Error while saving:', err);
      } else {
      }
    });
  });
}
