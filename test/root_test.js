var helpers = require('./helpers');
var models = require('../models');
var etag = require('../api_helper').etag;
var q = require('q');
var config = require('../config.json');
var supertest = require('supertest');
var should = require('chai').should();
var sch = require('./schemas');

test();

function test() {
  var api = supertest(config.tests.api);
  var user = config.tests.user;
  var password = config.tests.password;

  describe('In order to test Root resource', function() {
    it('run all the tests', function(done) {
      console.log('starting Root test');

      var cfg = {
        api: api,
        user: user,
        password: password,
        name: 'root',
        namePlural: 'recursos',
        path: '/'
      };

      helpers.testGetList(cfg);

      console.log('finishing Root test');
      done();
    });
  });
}
