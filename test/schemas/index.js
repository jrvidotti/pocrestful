var fs        = require('fs');
var sch = {};

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return ((file.indexOf('.') !== 0) && (file !== 'index.js') && (file.slice(-5) == '.json'));
  })
  .forEach(function(file) {
    sch[file.slice(0,-5)] = require('./' + file);
  });

var chai = require('chai');
chai.use(require('chai-json-schema'));
chai.tv4.addSchema('link', sch.link);
chai.tv4.addSchema('pais', sch.pais_basic);
chai.tv4.addSchema('estado', sch.estado_basic);
chai.tv4.addSchema('cidade', sch.cidade_basic);
chai.tv4.addSchema('bairro', sch.bairro_basic);
chai.tv4.addSchema('cep', sch.cep_basic);
chai.tv4.addSchema('pessoafisica', sch.pessoafisica_basic);

module.exports = sch;
