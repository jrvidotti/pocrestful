q = require('q');

function delay(ms) {
  var deferred = q.defer();
  setTimeout(deferred.resolve, ms);
  return deferred.promise;
}

console.log('1');
delay(1000)
  .then(function(){
    console.log('tadah!');
  });

delay(2000)
  .then(function(){
    console.log('pam!');
  });
console.log('2');

