var should = require('chai').should();
var checkCpf = require('../utils').checkCpf;

describe('Testing checkCPF', function() {
  it('valid CPF number returns true', function() {
    checkCpf('652....006.241-34').should.be.ok;
    checkCpf('652.006.241-34').should.be.ok;
    checkCpf('65200624134').should.be.ok;
    checkCpf(65200624134).should.be.ok;
    checkCpf('11111111111').should.be.ok;
  });
  it('invalid CPF number returns false', function() {
    checkCpf('65200624133').should.not.be.ok;
    checkCpf(65200624133).should.not.be.ok;
  });
});